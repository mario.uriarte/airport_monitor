package model;

public class Flight {
    private int id;
    private String hour;
    private String airline;
    private String code;
    private String destiny;
    private String estimatedTime;
    private String gate;
    private String observation;


    public Flight(int id, String hour, String airline, String code, String destiny, String estimatedTime, String gate, String observation) {
        this.id = id;
        this.hour = hour;
        this.airline = airline;
        this.code = code;
        this.destiny = destiny;
        this.estimatedTime = estimatedTime;
        this.gate = gate;
        this.observation = observation;
    }
    public Flight() {}


    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDestiny() {
        return destiny;
    }

    public void setDestiny(String destiny) {
        this.destiny = destiny;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public void show() {
        System.out.print("ID:"+this.id +" | " + this.hour + " | \t" + this.airline + " | \t" + this.code + " | \t" + this.destiny + " | \t" + this.estimatedTime + " | \t" + this.gate + " | \t" + this.observation+"\n");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
