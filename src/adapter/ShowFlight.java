package adapter;

import model.FlightDescription;

public class ShowFlight {

    public void showFlightLargeDescription(FlightDescription flight) {
        flight.show();
    }
}
