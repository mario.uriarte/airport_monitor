package observer;

import model.Flight;
import observer.publisher.NotificadorManager;

public class CenterMachine {

    public NotificadorManager events;

    public CenterMachine() {
        this.events = new NotificadorManager("Departure", "Arrival");
    }

    public void departure(Flight flight) {
        events.notify("Departure", flight);
    }

    public void arrival(Flight flight) {
        events.notify("Arrival", flight);
    }

}
