package observer.listeners;

import model.Flight;

public class ArrivalListener implements EventListener {

    @Override
    public void updateFlight(String eventType, Flight flight) {
//        System.out.println("Arrival Flight update");
        flight.show();
    }
}
