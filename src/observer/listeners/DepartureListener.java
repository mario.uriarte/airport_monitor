package observer.listeners;

import model.Flight;

public class DepartureListener implements EventListener {

    @Override
    public void updateFlight(String eventType, Flight flight) {
//        System.out.println("Departure flight update");
        flight.show();
    }
}
