package observer.listeners;

import model.Flight;

public interface EventListener {
    void updateFlight(String eventType, Flight flight);
}
