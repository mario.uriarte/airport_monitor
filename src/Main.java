import adapter.LatamDescriptionAdapter;
import adapter.ShowFlight;
import model.Flight;
import model.LatamDescription;
import observer.CenterMachine;
import observer.listeners.ArrivalListener;
import observer.listeners.DepartureListener;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String args[]) throws IOException {

        Flight departureP98009 = new Flight(1,"07:15","PERUVIAN","P9 8009","PUNTA CANA","    ","29","DELAYED");
        Flight departureAA1558 = new Flight(2,"07:45","AmericanAir","AA 1558","MIAMI","08:51","29","CLOSED");
        Flight departureLA237 = new Flight(3,"09:10","LATAM","LA 2377","SAO PAULO","09:10","32","BOARDING");
        Flight departureLA2418 = new Flight(4,"09:15","LATAM","LA 2418","R.JANEIRO","09:15","28","LAST CALL");
        Flight departureLA609 = new Flight(5,"09:15","LATAM","LA 609 ","SANTIAGO","11:00","26","DELAYED");
        Flight departureLA7901 = new Flight(6,"09:30","LATAM","LA 7901","B.AIRES","09:30","31","BOARDING");

        Flight arrivalVV751 = new Flight(21,"09:25","Payasito","VV 751","CUSCO","09:29"," ","CONFIRMED");
        Flight arrivalLA2273 = new Flight(22,"09:25","LATAM","LA 2273","CHICLAYO","09:32"," ","CONFIRMED");
        Flight arrivalLA2191 = new Flight(23,"09:30","LATAM","LA 2191","JAUJA","09:11","2","CONFIRMED");
        Flight arrivalP9231 = new Flight(24,"09:30","PERUVIAN","P9 231","AREQUIPA","09:31"," ","CONFIRMED");
        Flight arrivalLA2116 = new Flight(25,"09:40","LATAM","LA 2116","AREQUIPA","09:22"," ","CONFIRMED");
        Flight arrivalLA2235 = new Flight(26,"09:55","LATAM","LA 2235","IQUITOS","09:42"," ","CONFIRMED");

        CenterMachine machine = new CenterMachine();

        machine.events.subscribe("Arrival", new ArrivalListener());
        machine.events.subscribe("Departure", new DepartureListener());

        System.out.println("AIRPORT MONITOR\n");

        System.out.println(">>> DEPARTURES");
        machine.departure(departureP98009);
        machine.departure(departureAA1558);
        machine.departure(departureLA237);
        machine.departure(departureLA2418);
        machine.departure(departureLA609);
        machine.departure(departureLA7901);

        System.out.println("\n\n>>> ARRIVALS");
        machine.arrival(arrivalVV751);
        machine.arrival(arrivalLA2273);
        machine.arrival(arrivalLA2191);
        machine.arrival(arrivalP9231);
        machine.arrival(arrivalLA2116);
        machine.arrival(arrivalLA2235);
        System.out.println("> Enter ID\n");

        // Enter data using BufferReader
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        // Reading data using readLine
        String name = reader.readLine();

        // Printing the read line

        if (name.equals("DEP")) {
            Runtime.getRuntime().exec("clear");
            System.out.println("SHOW DEPARTURES");
            machine.departure(departureP98009);
            machine.departure(departureAA1558);
            machine.departure(departureLA237);
            machine.departure(departureLA2418);
            machine.departure(departureLA609);
            machine.departure(departureLA7901);
        } else if (name.equals("ARR")) {
            Runtime.getRuntime().exec("clear");
            System.out.println("SHOW ARRIVALS");
            machine.arrival(arrivalVV751);
            machine.arrival(arrivalLA2273);
            machine.arrival(arrivalLA2191);
            machine.arrival(arrivalP9231);
            machine.arrival(arrivalLA2116);
            machine.arrival(arrivalLA2235);
        }


        LatamDescriptionAdapter latamDescriptionAdapter =  new LatamDescriptionAdapter(new LatamDescription());

        ShowFlight showFlight = new ShowFlight();
        showFlight.showFlightLargeDescription(latamDescriptionAdapter);

    }
}
